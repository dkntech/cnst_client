package kr.co.dkntech.cnst;

import android.app.Application;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.text.TextUtils;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

import kr.co.dkntech.database.DatabaseHandler;
import kr.co.dkntech.utils.Define;
import kr.co.dkntech.volley.LruBitmapCache;
//import kr.co.dkntech.volley.MultiPartStack;

/**
 * Created by nurgoori on 15. 6. 19..
 */
public class AppController extends Application {

    private static AppController mInstance;
    String mMacAddress;
    String mPwd = "1234";
    private DatabaseHandler mDB;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private int mWidth;
    private int mHeight;

    static {
        com.android.volley.VolleyLog.DEBUG = true;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
//        setPwd(Utils.getUniqueID(this));
        mMacAddress = ((WifiManager) getSystemService(Context.WIFI_SERVICE)).getConnectionInfo().getMacAddress();
        mDB = new DatabaseHandler(this);
    }

    @Override
    public void onTerminate() {
        // TODO Auto-generated method stub
        super.onTerminate();
    }

    public String getMacAddress() {
        return mMacAddress;
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public void setPwd(String pwd) {
        mPwd = pwd;
    }

    public String getPwd() {
        return mPwd;
    }

    public DatabaseHandler getDB() {
        return mDB;
    }

    public RequestQueue getRequestMultipartQueue() {
//        mRequestQueue = Volley.newRequestQueue(getApplicationContext(), new MultiPartStack());
        return mRequestQueue;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {

            mImageLoader = new ImageLoader(this.mRequestQueue, new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        RetryPolicy policy = new DefaultRetryPolicy(Define.TIME_OUT_60, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);
        req.setTag(TextUtils.isEmpty(tag) ? Define.TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        RetryPolicy policy = new DefaultRetryPolicy(Define.TIME_OUT_60, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);
        req.setTag(Define.TAG);
        getRequestQueue().add(req);
    }

    public <T> void addToMultipartRequestQueue(Request<T> req) {
        RetryPolicy policy = new DefaultRetryPolicy(Define.TIME_OUT_30, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);
        req.setTag(Define.TAG);
        getRequestMultipartQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public int getScreenWidth() {
        return mWidth;
    }

    public int getScreenHeight() {
        return mHeight;
    }

    public void setScreenWidth(int width) {
        mWidth = width;
    }

    public void setScreenHeight(int height) {
        mHeight = height;
    }
}
