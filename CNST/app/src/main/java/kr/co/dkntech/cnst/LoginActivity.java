package kr.co.dkntech.cnst;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import kr.co.dkntech.utils.BackPressCloseHandler;
import kr.co.dkntech.utils.Utils;


public class LoginActivity extends ActionBarActivity {

    private BackPressCloseHandler mBackCloseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mBackCloseHandler = new BackPressCloseHandler(this);

        Button btn = (Button) findViewById(R.id.btnLogin);
        btn.setOnClickListener(mClickListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View arg0) {
            String msg = "";

            switch(arg0.getId()) {

                case R.id.btnLogin:
                    if (Utils.checkRootingDevice()) {
                        Toast.makeText(getApplicationContext(), "루팅 된 단말입니다. 더이상 작업을 진행 할 수 없습니다.", Toast.LENGTH_SHORT).show();

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                finish();
                            }
                        }, 1000);
                    } else {
//					checkVersion();
//                        registerInBackground();
                        moveMain();
                    }
                    break;
            }
        }
    };

    private void moveMain() {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        mBackCloseHandler.onBackPressed();
    }
}
