package kr.co.dkntech.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import kr.co.dkntech.cnst.AppController;
import kr.co.dkntech.utils.Define;

public class DatabaseHandler extends SQLiteOpenHelper {
	
	private static final String TAG = "DatabaseHandler";
	
	
//	private CryptManager mManager;
	private String mMessage = "";
	private long mProcessed, mSize;
	private String mPwd;
	private Context mContext;
	private SQLiteDatabase mDB;

	public DatabaseHandler(Context context) {
		super(context, Define.DATABASE_PATH + context.getPackageName() + Define.DATABASE_NAME, null, 1);
		mContext = context;
		mDB = this.getWritableDatabase();
		init();
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		createTable(db);
	}
	
	private void createTable(SQLiteDatabase db) {
		String CREATE_PHOTO_TABLE = "CREATE TABLE " + Define.TABLE_PHOTO + "(" 
//				+ Define.KEY_ID + " 	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"
//				+ Define.KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
				
				+ Define.KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ Define.KEY_PHOTO_ID + " TEXT ,"
				+ Define.KEY_LOGIN_INFO + " TEXT,"
				+ Define.KEY_PHOTO_TASK + " TEXT,"
				+ Define.KEY_PHOTO_KIND + " INTEGER NOT NULL,"
				+ Define.KEY_PHOTO_ENCRYPT + " INTEGER NOT NULL,"
				+ Define.KEY_PHOTO_PATH + " TEXT,"
				+ Define.KEY_PHOTO_ETC + " TEXT,"
				+ Define.KEY_PHOTO_WIDTH + " TEXT,"
				+ Define.KEY_PHOTO_HEIGHT + " TEXT,"
				+ Define.KEY_PHOTO_DATA + " TEXT" + ")";
				
		db.execSQL(CREATE_PHOTO_TABLE);

		String CREATE_TASK_TABLE = "CREATE TABLE " + Define.TABLE_TASK + "(" 
				
				+ Define.KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ Define.KEY_TASK_ID + " TEXT UNIQUE,"
				+ Define.KEY_TASK_CODE + " TEXT,"
				+ Define.KEY_TASK_NAME + " TEXT,"
				+ Define.KEY_TASK_START + " TEXT,"
				+ Define.KEY_TASK_END + " TEXT,"
				+ Define.KEY_TASK_COMPLETE + " TEXT" + ")";
		
		db.execSQL(CREATE_TASK_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + Define.TABLE_PHOTO);
		db.execSQL("DROP TABLE IF EXISTS " + Define.TABLE_TASK);
		onCreate(db);

	}
	
	public void close() {
		mDB.close();
	}

	private void init() {
		String macAddress = AppController.getInstance().getMacAddress();
		mPwd = AppController.getInstance().getPwd();
				
	}


	
	private void printMessage() {
	}


	public boolean deleteImageData(String imageId) {
		boolean result = false;
		SQLiteDatabase db = this.getReadableDatabase();
		
		if(db.delete(Define.TABLE_PHOTO, Define.KEY_PHOTO_ID + " = ?", new String[] { imageId }) > 0) {
			result = true;
		} 
		db.close();
		
		return result;
	}
	
	
	// Task
	public boolean insertTaskData(String taskId, String taskCode, String taskName, String taskStart, String taskEnd) {
		boolean result = false;

		mDB = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(Define.KEY_TASK_ID, taskId);
		values.put(Define.KEY_TASK_CODE, taskCode);
		values.put(Define.KEY_TASK_NAME, taskName);
		values.put(Define.KEY_TASK_START, taskStart);
		values.put(Define.KEY_TASK_END, taskEnd);
		values.put(Define.KEY_TASK_COMPLETE, "N");
		
		if (mDB.insert(Define.TABLE_TASK, null, values) < 0) {
			Log.e(TAG, "insertTaskData -> insert Error !!");
			
			if (mDB.update(Define.TABLE_TASK, values, Define.KEY_TASK_ID + " = ?", new String[] { taskId }) > 0) {
				Log.e(TAG, "insertTaskData -> update !!");
				result = true;
			} else {
				Log.e(TAG, "insertTaskData -> update Error !!");
				result = false;
			}
		} else {
			Log.d(TAG, "insertTaskData -> insert ok -> ");
			result = true;
		}

		mDB.close();
		
		return result;
	}
	
	public boolean updateTaskData(String taskId, String complete) {
		boolean result = false;

		mDB = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(Define.KEY_TASK_COMPLETE, complete);
			
		if (mDB.update(Define.TABLE_TASK, values, Define.KEY_TASK_ID + " = ?", new String[] { taskId }) > 0) {
			Log.e(TAG, "updateTaskData -> update !!");
			result = true;
		} else {
			Log.e(TAG, "updateTaskData -> update Error !!");
			result = false;
		}

		mDB.close();
		
		return result;
	}
	
	public boolean deleteTaskData(String taskId) {
		boolean result = false;
		SQLiteDatabase db = this.getReadableDatabase();
		
		if(db.delete(Define.TABLE_TASK, Define.KEY_TASK_ID + " = ?", new String[] { taskId }) > 0) {
			result = true;
		} 
		db.close();
		
		return result;
	}

}

















