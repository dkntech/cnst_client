package kr.co.dkntech.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by nurgoori on 15. 6. 23..
 */
public class CatalogueRequester {
    private HashMap<String, String> mHeaders = new HashMap<String, String>();
    private JSONObject mSendJson = new JSONObject();
    private String mUrl = "";
    static String mTag = "TAG_CATALOG";

    public interface CatalogueRequesterListener {
        public void onSendStart(String tag);
        public void onResponse(String tag, String result);
        public void onError(String result);
    }

    private CatalogueRequesterListener mCaller;

    public CatalogueRequester() {
//        mHeaders.put("test_key", "test");
    }

    public void setListener(CatalogueRequesterListener caller) {
        this.mCaller = caller;
    }

//    public void setHeadData(HashMap<String, String> headers) {
//        mHeaders = headers;
//    }
//
//    public void addHeadData(String key, String val) {
//        mHeaders.put(key, val);
//    }

    public void setBodyData(String key, String val) {
        try {
            mSendJson.put(key, val);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public HashMap<String, String> getHeaderData() {
        return  mHeaders;
    }

    public JSONObject getBodyData() {
        return mSendJson;
    }

    public void setUrl(String url) {
        mUrl = url;
    }


}
