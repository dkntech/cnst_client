package kr.co.dkntech.cnst;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import kr.co.dkntech.model.ExpandableListAdapter;

/**
 */
public class CategoryFragment extends android.support.v4.app.Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private Context mContext;
    private ExpandableListView mList;
    List<String> mGroupList = new ArrayList<String>();
//    private ArrayList<String> mGroupList = new ArrayList<String>();;
    private ArrayList<String> mChildListContent = null;
    private HashMap<String, List<String>> mChildList = new HashMap<String, List<String>>();
    private ExpandableListAdapter expandableAdapter;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CategoryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CategoryFragment newInstance(String param1, String param2) {
        CategoryFragment fragment = new CategoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public CategoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mChildListContent = new ArrayList<String>();

        View view = inflater.inflate(R.layout.fragment_category, container, false);

        mList = (ExpandableListView) view.findViewById(R.id.expandableListView);
        expandableAdapter = new ExpandableListAdapter(mContext, mGroupList, mChildList);
        mList.setAdapter(expandableAdapter);

        // 그룹 클릭 했을 경우 이벤트
        mList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                Toast.makeText(mContext, "g click = " + groupPosition,
                        Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        // 차일드 클릭 했을 경우 이벤트
        mList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                Toast.makeText(mContext, "c click = " + childPosition,
                        Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        // 그룹이 닫힐 경우 이벤트
        mList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
                Toast.makeText(mContext, "g Collapse = " + groupPosition,
                        Toast.LENGTH_SHORT).show();
            }
        });

        // 그룹이 열릴 경우 이벤트
        mList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                Toast.makeText(mContext, "g Expand = " + groupPosition,
                        Toast.LENGTH_SHORT).show();
            }
        });
        // Inflate the layout for this fragment
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
        ((MainActivity) activity).onSectionAttached(3);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void setArrangeListData() {
        mGroupList.add("피자");
        mGroupList.add("치킨");
        mGroupList.add("중국집");

        ArrayList<String> arrayPizza = new ArrayList<String>();
        arrayPizza.add("치즈");
        arrayPizza.add("고구마");
        arrayPizza.add("비싼거");
        arrayPizza.add("치즈");
        arrayPizza.add("고구마");
        arrayPizza.add("비싼거");

        ArrayList<String> arrayChicken = new ArrayList<String>();
        arrayChicken.add("후라이드");
        arrayChicken.add("양념");
        arrayChicken.add("간장");
        arrayChicken.add("후라이드");
        arrayChicken.add("양념");
        arrayChicken.add("간장");

        ArrayList<String> arrayCh = new ArrayList<String>();
        arrayCh.add("짜장");
        arrayCh.add("짬뽕");
        arrayCh.add("볶음밥");
        arrayCh.add("짜장");
        arrayCh.add("짬뽕");
        arrayCh.add("볶음밥");

        mChildList.put(mGroupList.get(0), arrayPizza);
        mChildList.put(mGroupList.get(1), arrayChicken);
        mChildList.put(mGroupList.get(2), arrayCh);
    }
}
