package kr.co.dkntech.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.util.Log;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import kr.co.dkntech.cnst.AppController;

public class Utils {

	public synchronized static int getPhotoOrientationDegree(String filepath) {
		int degree = 0;
		ExifInterface exif = null;

		try {
			exif = new ExifInterface(filepath);
		} catch (IOException e) {
		}

		if (exif != null) {
			int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, -1);

			if (orientation != -1) {
				switch (orientation) {
				case ExifInterface.ORIENTATION_ROTATE_90:
					degree = 90;
					break;

				case ExifInterface.ORIENTATION_ROTATE_180:
					degree = 180;
					break;

				case ExifInterface.ORIENTATION_ROTATE_270:
					degree = 270;
					break;
				}

			}
		}
		return degree;
	}
	
	public synchronized static Bitmap getRotatedBitmap(Bitmap bitmap, int degrees) {
		if (degrees != 0 && bitmap != null) {
			Matrix m = new Matrix();
			m.setRotate(degrees, (float) bitmap.getWidth() / 2, (float) bitmap.getHeight() / 2);
			try {
				Bitmap b2 = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), m, true);
				if (bitmap != b2) {
					bitmap.recycle();
					bitmap = b2;
				}
			} catch (OutOfMemoryError e) {
			}
		}

		return bitmap;
	}
	
	public synchronized static byte[] convertByteArray(File file) {
		if (file.exists()) {
			byte[] data = null;
			try {
				data = FileUtils.readFileToByteArray(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return data;
//			BitmapFactory.Options options = new BitmapFactory.Options();
//			options.inSampleSize = 2; // 8
//			Bitmap bitmap = BitmapFactory.decodeFile(file.getPath(), options);
//			if (bitmap != null) {
//				ByteArrayOutputStream stream = new ByteArrayOutputStream();
//				bitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream);
//				return stream.toByteArray();	
//			}
		} 
			
		return null;
	}
	
//	public synchronized static File convertFile(byte[] data, String path) {
//		File file = null;
//		FileOutputStream fo;
//		file = new File(path);
//		try {
////			FileUtils.writeByteArrayToFile(file, data);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
////		ByteArrayOutputStream stream = new ByteArrayOutputStream();
////		Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
////		bmp.compress(Bitmap.CompressFormat.JPEG, 90, stream);
//
////		try {
////			fo = new FileOutputStream(file);
////			fo.write(stream.toByteArray());
////			fo.close();
////		} catch (IOException e) {
////			e.printStackTrace();
////		}
//
//		return file;
//	}
	
	public static Uri convertExtension(Uri fileUri){
		String filePath = fileUri.toString();
		int fileIdx = filePath.lastIndexOf(".");
		String path = filePath.substring(0, fileIdx);
		path = path + ".locked";
		Uri result = Uri.parse(path);
		return result;
	}
	
	public static File convertExtension(File file){
		String filePath = file.getPath();
		int fileIdx = filePath.lastIndexOf(".");
		String path = filePath.substring(0, fileIdx);
		path = path + ".locked";
		File renameFile = new File(path);
		return renameFile;
	}
	
	public static String getFlatString(String date) {
		return date.replace("-", "").replace(":", "").replace(" ", "").replace("/", "").replace(",", "");
	}
	
	public static String getCurrentTime() {
		SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss", Locale.KOREA);
		Date currentTime = new Date();
		return getDateString(currentTime, null);
	}
	
	public static String getDateString(Date date, String format) {
		if (format == null)
			format = "yyyy-MM-dd kk:mm:ss";

		SimpleDateFormat df = new SimpleDateFormat(format);
		return df.format(date);
	}
	

	/* 문자열을 압축한 문자로 변환 */
	public static String compressString(String string)  throws IOException {
		return new String(Base64.encodeBase64(compress(string)));
//		return new String(Base64.encode((compress(string)), Base64.NO_WRAP));
	}
	
	/* 압축된 문자열을 해제된 문자로 변환 */
	public static String decompressString(String string)  throws IOException {
		return decompress(Base64.decodeBase64(string.getBytes()));
//		return new String(Base64.decode(string.getBytes(), Base64.NO_WRAP));
	}
	
	public static byte[] compress(String string) throws IOException {
	    byte[] blockcopy = ByteBuffer
	        .allocate(4)
	        .order(java.nio.ByteOrder.LITTLE_ENDIAN)
	        .putInt(string.length())
	        .array();
	    ByteArrayOutputStream os = new ByteArrayOutputStream(string.length());
	    GZIPOutputStream gos = new GZIPOutputStream(os);
	    gos.write(string.getBytes());
	    gos.close();
	    os.close();
	    byte[] compressed = new byte[4 + os.toByteArray().length];
	    System.arraycopy(blockcopy, 0, compressed, 0, 4);
	    System.arraycopy(os.toByteArray(), 0, compressed, 4, os.toByteArray().length);
	    
	    return compressed;
	}

	public static String decompress(byte[] compressed) throws IOException {
	    final int BUFFER_SIZE = 32;
	    ByteArrayInputStream is = new ByteArrayInputStream(compressed, 4, compressed.length - 4);
	    GZIPInputStream gis = new GZIPInputStream(is, BUFFER_SIZE);
	    StringBuilder string = new StringBuilder();
	    byte[] data = new byte[BUFFER_SIZE];
	    int bytesRead;
	    while ((bytesRead = gis.read(data)) != -1) {
	        string.append(new String(data, 0, bytesRead));
	    }
	    gis.close();
	    is.close();
	    return string.toString();
	}
	
	/**
	 * 파일을 BASE64Encoding 문자열로 변환
	 * @param fullpath
	 * @return
	 * @throws Exception
	 */
	public static String getBase64String(String fullpath) throws Exception {
		try{
			byte[] io = getBytes(fullpath);
	        return new String(Base64.encodeBase64(io));
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public static String getBase64String(byte[] io) throws Exception {
		try{
//			byte[] io = getBytes(fullpath);
	        return new String(Base64.encodeBase64(io));
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	

	private static byte[] getBytes(String fullPath) throws Exception {
		
		File file = new File(fullPath);
		if(file.exists()){
			FileInputStream fis = new FileInputStream(file);

			return IOUtils.toByteArray(fis);
		}else{
			return null;
		}

	}
//	/**
//	 * 압축된 Base64를 압축해제하여 파일로 저장한다.
//	 * @param base64
//	 * @return
//	 */
//	public static String getFileFromBase64(String compressBase64) throws Exception {
//		//1. 압축해제 수행
//		compressBase64 = decompressString(compressBase64);
//		//2. Base64문자열을 파일로 변환
//		String fpath = Define.IMAGE_PATH;// + DateUtil.getCurrentYyyymmdd() +"/";
//		String fname = String.valueOf(System.nanoTime());
//		writeFile(compressBase64, fpath, fname);
//
//		return fpath + fname;
//	}
	/**
	 * Base64문자열을 지정하는 위치의 파일명으로 저장한다.
	 * @param base64
	 * @param path
	 * @param fname
	 * @throws Exception
	 */
	private static void writeFile(String base64, String path, String fname) throws Exception {
		FileOutputStream fos = null;
		try {
			mkdir(path); 
		    byte[] newb = Base64.decodeBase64(base64.getBytes());
		    fos = new FileOutputStream(path+fname);
		    fos.write(newb);
		    fos.flush();
//			logger.debug("file create : " + path + fname);
		    Log.d("cryptdemo", "file create : " + path + fname);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			IOUtils.closeQuietly(fos);
		}// try end;
	}
	private static void mkdir(String path){
		File file = new File(path);
		if(!file.exists()){
			file.mkdirs();
		}
	}
	
	public static String getImagePath(Uri uri) {
		Cursor cursor = AppController.getInstance().getContentResolver().query(uri, null, null, null, null);
		cursor.moveToFirst();
		String document_id = cursor.getString(0);
		document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
		cursor.close();

		cursor = AppController.getInstance().getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null,
				MediaStore.Images.Media._ID + " = ? ", new String[] { document_id }, null);
		cursor.moveToFirst();
		String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
		cursor.close();

		return path;
	}
	
	public static byte[] bitmapToByteArray( Bitmap bitmap ) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream() ;
        bitmap.compress( CompressFormat.JPEG, 100, stream) ;
        byte[] byteArray = stream.toByteArray() ;
        return byteArray ;  
    }  
	
	public static final String ROOT_PATH = Environment.getExternalStorageDirectory() + "";
	public static final String ROOTING_PATH_1 = "/system/bin/su";
	public static final String ROOTING_PATH_2 = "/system/xbin/su";
	public static final String ROOTING_PATH_3 = "/system/app/SuperUser.apk";
	public static final String ROOTING_PATH_4 = "/data/data/com.noshufou.android.su";

	public static String[] RootFilesPath = new String[] { ROOT_PATH + ROOTING_PATH_1, ROOT_PATH + ROOTING_PATH_2, ROOT_PATH + ROOTING_PATH_3,
			ROOT_PATH + ROOTING_PATH_4 };

	/**
	 * 루팅된 디바이스인지 체크.
	 * 
	 * @return
	 */
	public static boolean checkRootingDevice() {
		boolean isRootingFlag = false;

		try {
			Process process = Runtime.getRuntime().exec("find / -name su");

			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			if (reader.ready() == false) {
				return false;
			}

			String result = reader.readLine();
			if (result.contains("/su") == true) {
				isRootingFlag = true;
			}

		} catch (Exception e) {
			isRootingFlag = false;
		}

		if (!isRootingFlag) {
			isRootingFlag = checkRootingFiles(RootFilesPath);
		}

		return isRootingFlag;
	}

	/**
	 * 루팅 파일이 존재하는지 체크.
	 * 
	 * @param
	 * @return
	 */
	private static boolean checkRootingFiles(String[] filePaths) {
		boolean result = false;
		File file;

		for (String path : filePaths) {
			file = new File(path);

			if (file != null && file.exists() && file.isFile()) {
				result = true;
				break;
			} else {
				result = false;
			}
		}
		return result;
	}

	public static String getUniqueID(Context context) {
        final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        final String tmDevice, tmSerial, androidId;
        tmDevice = "" + tm.getDeviceId();
        tmSerial = "" + tm.getSimSerialNumber();
        androidId = "" + android.provider.Settings.Secure.getString(context.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);

        UUID deviceUuid = new UUID(androidId.hashCode(), ((long)tmDevice.hashCode() << 32) | tmSerial.hashCode());
        String deviceId = deviceUuid.toString();
        return deviceId;
    }
	
	public static String getAppVersion(Context context) {
		String mAppVersion = "";
		/* get app version */
		try {
			PackageInfo pi = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			mAppVersion = pi.versionName;
			Log.i(Define.TAG, "App version: " + mAppVersion);
		} catch (NameNotFoundException e) {
			Log.i(Define.TAG, "Not Found Version Name");
		}
		return mAppVersion;
	}

	/**
	 * App Version 을 비교한다.
	 * 
	 * @return int 버전이 같으면 0, 비교 버전이 크면 1, 비교 버전이 작으면 -1 Error 99
	 */
	public static int compareToVersion(String version, String compareVersion) {
		try {
			if (version != null && compareVersion != null) {
				String[] array_version = version.split("\\.");
				String[] array_compareVersion = compareVersion.split("\\.");

				int compareArrayCount = 0;

				if (array_version.length > array_compareVersion.length) {
					compareArrayCount = array_compareVersion.length;
				} else if (array_version.length < array_compareVersion.length) {
					compareArrayCount = array_version.length;
				} else {
					compareArrayCount = array_version.length;
				}

				for (int i = 0; i < compareArrayCount; i++) {

					int v_num = Integer.parseInt(array_version[i]);
					int c_num = Integer.parseInt(array_compareVersion[i]);

					if (v_num < c_num) {
						return 1;
					} else if (v_num > c_num) {
						return -1;
					}
				}

				if (array_version.length > compareArrayCount) {
					for (int i = compareArrayCount; i < array_version.length; i++) {

						int num = Integer.parseInt(array_version[i]);

						if (num > 0) {
							return -1;
						}
					}
				}

				if (array_compareVersion.length > compareArrayCount) {
					for (int i = compareArrayCount; i < array_compareVersion.length; i++) {

						int num = Integer.parseInt(array_compareVersion[i]);

						if (num > 0) {
							return 1;
						}
					}
				}
			} else {
				return 99;
			}
			return 0;
		} catch (Exception e) {
//			PLog.i(CLASS_TAG, "compareToVersion Exception Error");
			return 99;
		}
	}
	
	/**
	 * 로컬 저장소에 어플리케이션의 환경 변수를 저장한다. 
	 * @param name 변수 이름
	 * @param value 변수 값
	 * @param ctx Context
	 */
	public static void setVariableToStorage(String name, String value, Context ctx) {
		SharedPreferences preferences = ctx.getSharedPreferences(Define.TAG, Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString(name, value);
		editor.commit();
	}
	
	/**
	 * 로컬 저장소에서 어플리케이션의 환경 변수 값을 읽어온다.
	 * @param name 변수 이름
	 * @param ctx Context
	 * @return 환경 변수값
	 */
	public static String getVariableFromStorage(String name, Context ctx){
		SharedPreferences preferences = ctx.getSharedPreferences(Define.TAG, Activity.MODE_PRIVATE);
    	return preferences.getString(name, "");
	}
}
