package kr.co.dkntech.model;

/**
 * Created by nurgoori on 15. 6. 22..
 */
public class CatalogueInfo {
    private String mImgURL;
    private String mPrice;
    private String mTitle;
    private String mGoodsID;

    public CatalogueInfo() {
    }

    public CatalogueInfo(String imgURL, String price) {
        this.mImgURL = imgURL;
        this.mPrice = price;
    }

    public String getImgURL() {
        return this.mImgURL;
    }

    public void setImgURL(String imgURL) {
        this.mImgURL = imgURL;
    }

    public String getPrice() {
        return this.mPrice;
    }

    public void setPrice(String price) {
        this.mPrice = price;
    }

    public String getTtile() {
        return this.mTitle;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public String getGoodsID() {
        return this.mGoodsID;
    }

    public void setGoodsID(String goodsID) {
        this.mGoodsID = goodsID;
    }
}
