package kr.co.dkntech.utils;

import android.graphics.BitmapFactory;
import android.os.Environment;



public class Define {

	public static final String TAG = "cnst";

	public static final String TAG_CATALOGUE_POP = "CATALOGUE_POP";
	public static final String TAG_CATALOGUE_REC = "CATALOGUE_REC";
	
	public static final int CRYPT_QUICK = 1;
	public static final int CRYPT_COMPLETE = 2;
	
	public static final int IMAGE_TYPE_PIC = 1;
	public static final int IMAGE_TYPE_SIGN = 2;
	
	public static final int DIALOG_WIDTH = 1000;
	public static final int DIALOG_HEIGHT = 600;
	
	public static final int TIME_OUT_30 = 3000 * 10;
	public static final int TIME_OUT_60 = 6000 * 10;
	
	public static final String DATABASE_PATH = 
			Environment.getExternalStorageDirectory().getAbsolutePath() + "/Android/data/";
	public static final String DATABASE_NAME = "/cnst.db";
	
	public static final String IMAGE_PATH = 
			Environment.getExternalStorageDirectory().getAbsolutePath() + 
			"/Android/data/kr.co.dkntech.cnst/image/";
	

	public static final String TABLE_PHOTO = "photo_datas";
	public static final String KEY_ID = "_id";
	public static final String KEY_PHOTO_ID = "photo_id";
	public static final String KEY_LOGIN_INFO = "login_info";
	public static final String KEY_PHOTO_TASK = "photo_task";
	public static final String KEY_PHOTO_ETC = "photo_etc";
	public static final String KEY_PHOTO_KIND = "photo_kind"; // 1 photo, 2 sign
	public static final String KEY_PHOTO_ENCRYPT = "photo_encrypt";
	public static final String KEY_PHOTO_PATH = "photo_path";
	public static final String KEY_PHOTO_DATA = "photo_data";
	public static final String KEY_PHOTO_WIDTH = "photo_width";
	public static final String KEY_PHOTO_HEIGHT = "photo_height";
	
	public static final String TABLE_TASK = "task_datas";
	public static final String KEY_TASK_ID = "task_id";
	public static final String KEY_TASK_CODE = "task_code";
	public static final String KEY_TASK_NAME = "task_name";
	public static final String KEY_TASK_START = "task_start";
	public static final String KEY_TASK_END = "task_end";
	public static final String KEY_TASK_COMPLETE = "task_complete";
	
	public static final String JSON_HEAD = "JSON_HEAD";
	public static final String JSON_RESULT_CODE = "result_state";
	public static final String JSON_RESULT_MSG = "result_message";
	public static final String JSON_RESULT = "result";
	public static final String STR_RESULT_SUCCESS = "STR_RESULT_SUCCESS";
	public static final String JSON_BODY = "JSON_BODY";
	
	
	public static final String SERVER_URL = "http://dkntech.iptime.org:8080/";
//
	
	public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height >= reqHeight || width >= reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        
        return inSampleSize;
	}
}

