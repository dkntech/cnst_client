package kr.co.dkntech.model;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;

import kr.co.dkntech.cnst.AppController;
import kr.co.dkntech.cnst.GoodsDetailActivity;
import kr.co.dkntech.cnst.R;

/**
 * Created by nurgoori on 15. 6. 22..
 */
public class BestImageAdapter extends PagerAdapter {
    private Activity mActivity;
    private ArrayList<CatalogueInfo> mImagePaths;
    private LayoutInflater inflater;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    // constructor
    public BestImageAdapter(Activity activity, ArrayList<CatalogueInfo> imagePaths) {
        this.mActivity = activity;
        this.mImagePaths = imagePaths;
    }

    @Override
    public int getCount() {
        return this.mImagePaths.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
//        TouchImageView imgDisplay;
//        Button btnClose, btnRetake;
//        TextView txtCount;

        inflater = (LayoutInflater) mActivity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.layout_best_image, container, false);

//        imgDisplay = (TouchImageView) viewLayout.findViewById(R.id.imgDisplay);
//        btnClose = (Button) viewLayout.findViewById(R.id.btnClose);
//        btnRetake = (Button) viewLayout.findViewById(R.id.btnRetake);
//        txtCount = (TextView) viewLayout.findViewById(R.id.txtCount);
//        String count = String.format("%d/%d", position+1, getCount());
//        txtCount.setText(count);

        CatalogueInfo info = mImagePaths.get(position);
        NetworkImageView thumView = (NetworkImageView) viewLayout.findViewById(R.id.imgDisplay);
        thumView.setImageUrl(info.getImgURL(), imageLoader);

        // close button click event
//        btnClose.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mActivity.finish();
//            }
//        });

        viewLayout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, GoodsDetailActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mActivity.startActivity(intent);
            }
        });

        ((ViewPager) container).addView(viewLayout);

        return viewLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

//		TouchImageView imgDisplay = (TouchImageView) ((ViewPager) container).findViewById(R.id.imgDisplay);
//
//		if(imgDisplay != null) {
//			BitmapDrawable bmpDrawable = (BitmapDrawable) imgDisplay.getDrawable();
//			Bitmap bmp = bmpDrawable.getBitmap();
//		    if (bmpDrawable != null && bmp != null) {
//		    		bmp.recycle();
//		    		bmp = null;
//		            Log.d("cryptdemo", "recycle = " + position);
////		            bmpDrawable.setCallback(null);
//		    }
//		}

//        Log.d("cryptdemo", "position = " + position);
        ((ViewPager) container).removeView((View) object);
    }
}
