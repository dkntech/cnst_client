package kr.co.dkntech.utils;

import android.app.Activity;
import android.widget.Toast;

public class BackPressCloseHandler {
	private long backKeyPressedTime = 0;
	private Toast mToast;
	private Activity mActivity;

	public BackPressCloseHandler(Activity context) {
		this.mActivity = context;
	}

	public void onBackPressed() {
		if (System.currentTimeMillis() > backKeyPressedTime + 2000) {
			backKeyPressedTime = System.currentTimeMillis();
			showGuide();
			return;
		}
		if (System.currentTimeMillis() <= backKeyPressedTime + 2000) {
			
//			for (Map.Entry<String, Activity> entry : AppController.sActivityList.entrySet()) {
//				if (entry.getValue() != null) {
//					Log.d(Define.TAG, "entry key = " + entry.getKey() );
//					entry.getValue().finish();
//				}
//			}
//			
//			Intent intent = new Intent(mActivity.getApplicationContext(), LogoutService.class);
//			mActivity.stopService(intent);
			
			mActivity.finish();
			mToast.cancel();
		}
	}

	private void showGuide() {
		mToast = Toast.makeText(mActivity, "\'뒤로\'버튼을 한번 더 누르시면 종료됩니다.", Toast.LENGTH_SHORT);
		mToast.show();
	}
}
