//package kr.co.dkntech.volley;
//
//import android.util.Log;
//
//import com.android.volley.NetworkResponse;
//import com.android.volley.ParseError;
//import com.android.volley.Response;
//import com.android.volley.toolbox.HttpHeaderParser;
//import com.android.volley.toolbox.JsonRequest;
//
//import org.apache.http.Consts;
//import org.apache.http.HttpEntity;
//import org.apache.http.entity.ContentType;
//import org.apache.http.entity.mime.HttpMultipartMode;
//import org.apache.http.entity.mime.MultipartEntityBuilder;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.File;
//import java.io.UnsupportedEncodingException;
//import java.util.HashMap;
//import java.util.Map;
//
//import kr.co.dkntech.utils.Define;
//
//
//public class MultipartRequest extends JsonRequest<JSONObject> {
//    private  Map<String, String> mStringParts = new HashMap<String, String>();
//    private  Map<String, File> mFileParts = new HashMap<String, File>();
//    private  Map<String, String> mListParts = new HashMap<String, String>();
//    private MultipartEntityBuilder mBuilder = MultipartEntityBuilder.create();
//
//    public MultipartRequest(String url,
//                            Map<String, String> stringParts,
//                            Map<String, File> fileParts,
//                            Map<String, String> listParts,
//                            Response.Listener<JSONObject> listener,
//                            Response.ErrorListener errorListener) {
//        super(Method.POST, url, null, listener, errorListener);
//        mStringParts = stringParts;
//        mFileParts = fileParts;
//        mListParts = listParts;
//        buildMultipartEntity();
//    }
//
//    private void buildMultipartEntity() {
//        mBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
//        mBuilder.setBoundary(Long.toString(System.currentTimeMillis()));
//        mBuilder.setCharset(Consts.UTF_8);
//
//        for ( Map.Entry<String, String> entry : mStringParts.entrySet() ) {
//         mBuilder.addTextBody(entry.getValue() , mListParts.get(entry.getKey()));
//        }
//
//        for ( Map.Entry<String, File> entry : mFileParts.entrySet() ) {
//            ContentType imageContentType = ContentType.create("image/jpeg");
//            mBuilder.addBinaryBody("file", entry.getValue(), imageContentType, entry.getKey());
//        }
//    }
//
//    @Override
//	public byte[] getBody() {
//    		Log.d(Define.TAG, ">>>>>>>>>>> getBody >>>>>>>>>>>" );
//		Log.d(Define.TAG, new String(getBody()));
//		Log.d(Define.TAG, ">>>>>>>>>>>>>>>>>>>>>>" );
//		return super.getBody();
//	}
//
//	@Override
//    public String getBodyContentType() {
//		Log.d(Define.TAG, ">>>>>>>>>>> getBodyContentType >>>>>>>>>>>" );
//		Log.d(Define.TAG, mBuilder.build().getContentType().getValue() );
//		Log.d(Define.TAG, ">>>>>>>>>>>>>>>>>>>>>>" );
//        return mBuilder.build().getContentType().getValue();
//    }
//
//    public HttpEntity getEntity() {
//    		Log.d(Define.TAG, ">>>>>>>>>>> getEntity >>>>>>>>>>>" );
//		Log.d(Define.TAG, mBuilder.build().toString());
//		Log.d(Define.TAG, ">>>>>>>>>>>>>>>>>>>>>>" );
//        return mBuilder.build();
//    }
//
//    @Override
//    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
//        try {
//            String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
//            return Response.success(new JSONObject(jsonString), HttpHeaderParser.parseCacheHeaders(response));
//        } catch (UnsupportedEncodingException e) {
//            return Response.error(new ParseError(e));
//        } catch (JSONException je) {
//            return Response.error(new ParseError(je));
//        }
//    }
//
//}