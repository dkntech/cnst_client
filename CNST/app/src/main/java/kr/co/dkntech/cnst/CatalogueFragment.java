package kr.co.dkntech.cnst;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import kr.co.dkntech.model.BestImageAdapter;
import kr.co.dkntech.model.CatalogueInfo;
import kr.co.dkntech.model.CatalogueRequester;
import kr.co.dkntech.utils.Define;


/**
 */
public class CatalogueFragment extends android.support.v4.app.Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private PullToRefreshListView mListView;
    private CatalogueAdapter mAdapter;
    private Context mContext;
    private ViewPager mViewPager;
    private BestImageAdapter mBestAdapter;
    private ArrayList<CatalogueInfo> m_goods;
    private ArrayList<CatalogueInfo> mArrayViewPage;
    private int mPage = 1;
    private ScrollViewEx mScroll;
    private final int REQ_POP = 1;
    private final int REQ_REC = 2;

    String TAG = "";


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CatalogueFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CatalogueFragment newInstance(String param1, String param2) {
        CatalogueFragment fragment = new CatalogueFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public CatalogueFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

//        mPDialog = new ProgressDialog(mContext);
//        mPDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//        mPDialog.setMessage(getResources().getString(R.string.wait_please));

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_catalogue, container, false);
        m_goods = new ArrayList<CatalogueInfo>();
        mArrayViewPage = new ArrayList<CatalogueInfo>();
        mScroll = (ScrollViewEx) view.findViewById(R.id.scrollView);
        mScroll.setOnBottomReachedListener(
                new ScrollViewEx.OnBottomReachedListener() {
                    @Override
                    public void onBottomReached() {
//                        Toast.makeText(mContext, "End of ScrollView !",
//                                Toast.LENGTH_SHORT).show();

                        requestRecommend();
                    }
                }
        );

        mViewPager = (ViewPager) view.findViewById(R.id.pager);
        mBestAdapter = new BestImageAdapter((Activity) mContext, mArrayViewPage);
        mViewPager.setAdapter(mBestAdapter);
        mViewPager.setCurrentItem(0);

        CirclePageIndicator indicator = (CirclePageIndicator) view.findViewById(R.id.indicator);
        indicator.setViewPager(mViewPager);

        mAdapter = new CatalogueAdapter(mContext, R.layout.listview_catalogue_row, m_goods);
        mListView = (PullToRefreshListView) view.findViewById(R.id.listView_catalogue);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(Define.TAG, "clicked id = " + (int) id);
//                mActivePosition = (int) id;
//                mAdapter.notifyDataSetChanged();
//                displayButton();
//                requestData(Define.V_ASSIGN_DETAIL);
            }
        });

        mListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
            @Override
            public void onRefresh(PullToRefreshBase<ListView> arg0) {
                Log.d(Define.TAG, "onRefresh !!");
//                mPageIndex = 1;
//                mNextRow = true;
//                requestData(Define.V_ASSIGN_LIST);
            }
        });

        mListView.setOnLastItemVisibleListener(new PullToRefreshBase.OnLastItemVisibleListener() {
            @Override
            public void onLastItemVisible() {
                Log.d(Define.TAG, "onLastItemVisible !!");
                Toast.makeText(mContext, "End of List!",
                        Toast.LENGTH_SHORT).show();
            }
        });

        setListViewHeightBasedOnChildren(mListView);

        requestPopular();
        requestRecommend();

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
        ((MainActivity) activity).onSectionAttached(1);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();

        mScroll.smoothScrollTo(0, 0);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    public void setListViewHeightBasedOnChildren(PullToRefreshListView listView) {
        ListAdapter listAdapter =  mAdapter;//listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public void refreshListViewHeightBasedOnChildren(PullToRefreshListView listView) {
        ListAdapter listAdapter =  mAdapter;//listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight;
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public void requestPopular() {

        final ProgressDialog pDialog = ProgressDialog.show(mContext, null, null, true, false);
        pDialog.setContentView(R.layout.layout_progress);

        String url = "";

        CatalogueRequester rq = new CatalogueRequester();
        rq.setBodyData("id", "test_id");
        String volleyTag = Define.TAG_CATALOGUE_POP;
        url = String.format(Define.SERVER_URL + "CNST/m/catalogue/popular");

        final HashMap<String, String> headers = rq.getHeaderData();

        final JSONObject sendJson = rq.getBodyData();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(Define.TAG, "response" + response.toString());
                try {
                    String strResultCode = response.getString(Define.JSON_RESULT_CODE);
                    String strResultMsg = response.getString(Define.JSON_RESULT_MSG);

                    if (strResultCode.contentEquals("0000")) {
                            setPopulationView(response.getString(Define.JSON_RESULT));
                    } else {
                        Log.d(Define.TAG, strResultMsg);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                pDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(Define.TAG, "Error: " + error.getMessage());
                if (mPage > 1)  { mPage--; }

                pDialog.dismiss();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }

            @Override
            public byte[] getBody() {

                String sendData = sendJson.toString();
                try {
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return sendData.toString().getBytes();
            }
        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, volleyTag);
    }

    public void requestRecommend() {
        final ProgressDialog pDialog = ProgressDialog.show(mContext, null, null, true, false);
        pDialog.setContentView(R.layout.layout_progress);

        String url = "";

        CatalogueRequester rq = new CatalogueRequester();
        rq.setBodyData("id", "test_id");
        String volleyTag = Define.TAG_CATALOGUE_REC;
        rq.setBodyData("locale", "kr");
        rq.setBodyData("currency", "won");
        url = String.format(Define.SERVER_URL + "CNST/m/catalogue/recommend/pages/%d", mPage++);

        final HashMap<String, String> headers = rq.getHeaderData();

        final JSONObject sendJson = rq.getBodyData();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(Define.TAG, "response" + response.toString());
                try {
                    String strResultCode = response.getString(Define.JSON_RESULT_CODE);
                    String strResultMsg = response.getString(Define.JSON_RESULT_MSG);

                    if (strResultCode.contentEquals("0000")) {
                         setRecommendList(response.getString(Define.JSON_RESULT));
                    } else {
                        Log.d(Define.TAG, strResultMsg);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                pDialog.dismiss();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(Define.TAG, "Error: " + error.getMessage());
                if (mPage > 0)  { mPage--; }

                pDialog.dismiss();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }

            @Override
            public byte[] getBody() {

                String sendData = sendJson.toString();
                try {
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return sendData.toString().getBytes();
            }

            @Override
            public Object getTag() {
                return super.getTag();
            }
        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, volleyTag);
    }

    public void setPopulationView(String result) {
//        Gson gson = new Gson();
//        JsonParser jParser = new JsonParser();
//        JsonObject rootObejct = jParser.parse(result).getAsJsonObject();
//        JsonElement results = rootObejct.get("result");

        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(result);

            for(int i=0; i<jsonArray.length(); i++) {
                JSONObject item = jsonArray.getJSONObject(i);
//                JsonObject item = ((JsonObject) jsonArray.get(i)).getAsJsonObject();
                CatalogueInfo info = new CatalogueInfo();
                info.setImgURL(item.get("image_url").toString());
                mArrayViewPage.add(info);
            }
            mBestAdapter.notifyDataSetChanged();
            mViewPager.setCurrentItem(0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setRecommendList(String result) {

//        Gson gson = new Gson();
//        JsonParser jParser = new JsonParser();
//        JsonObject rootObejct = jParser.parse(result).getAsJsonObject();
//        JsonElement results = rootObejct.get("result");

        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(result);

            for(int i=0; i<jsonArray.length(); i++) {
                JSONObject item = jsonArray.getJSONObject(i);
                CatalogueInfo info = new CatalogueInfo();
                info.setImgURL(item.get("image_url").toString());
                info.setTitle(item.get("goods_name").toString());
                JSONArray options = new JSONArray(item.get("options").toString());
                JSONObject firstOption = options.getJSONObject(0);
                info.setPrice(firstOption.get("price").toString());
                mAdapter.add(info);
            }
            refreshListViewHeightBasedOnChildren(mListView);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //
    private class CatalogueAdapter extends ArrayAdapter<CatalogueInfo> {

        private Context mContext;
        private ArrayList<CatalogueInfo> mItems;
        private int mIndex;
        ImageLoader imageLoader = AppController.getInstance().getImageLoader();

        public CatalogueAdapter(Context context, int textViewResourceId, ArrayList<CatalogueInfo> items) {
            super(context, textViewResourceId, items);
            this.mItems = items;
            this.mContext = context;
            this.mIndex = 0;
        }

        public void addData(CatalogueInfo info) {
            add(info);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.listview_catalogue_row, null);
            }
            CatalogueInfo info = mAdapter.getItem(position);//mItems.get(position);

            if (info != null) {
                NetworkImageView thumView = (NetworkImageView) v.findViewById(R.id.imgGood);
                TextView txtTitle = (TextView) v.findViewById(R.id.txtTitle);
                TextView txtPrice = (TextView) v.findViewById(R.id.txtprice);
                thumView.setImageUrl(info.getImgURL(), imageLoader);
                txtTitle.setText(info.getTtile());
                txtPrice.setText(info.getPrice());
                txtPrice.setTextColor(getResources().getColor(R.color.orange));
            }

            return v;
        }
    }
}
